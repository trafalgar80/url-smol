## URL Short Test (Smol URL)
### Requirements:
- PHP 7.x
- sqlite and related PHP sqlite modules
- Composer
### Installation & Setup
To run the app:

    git clone https://bitbucket.org/trafalgar80/url-smol.git

Then run:

    cd url-smol
    composer install

Create the sqlite database (file):

    touch database/database.sqlite

Update the .env file params, for demo purposes you can leave as is for the below, but DB_DATABASE must be an absolute path to the sqlite database file:

    APP_URL=http://localhost:8000/ 
    APP_PREFIX=smol-url/
    DB_DATABASE=/absolute/path/to/database/database.sqlite

Run the migrations and seed the database:

    php artisan migrate
    php artisan db:seed

Now run PHP's built in server to demo the app (should match the APP_URL in the .env file:

    php -S localhost:8000 -t public

Finally navigate to http://localhost:8000 (or your APP_URL choice) in your browser.

To run some tests:

    vendor/bin/phpunit
    

Installation and setup tested on Windows 10 64bit, PHP 7.3.1 and with mingw.

### Solution
- Lumen/Laravel as I haven't used it in a while. It's really cool.
- REST API approach, easier to test and I could play around with different front-end/React, but due to time ended up using some boilerplate jQuery and Bootstrap.
- Algorithm, initial assumption for URL shortening was to use a random string generator/uniqid and having to check the database if that was in use.  This is not very efficient or scalable.  How can I avoid checking if it was in use? I could use a counter (increment after each new URL insertion) to base the shortened URL from, and use hex to convert the number to an alphanumeric - presenting more combinations in fewer digits (crucial in shortening!).  But wait, instead of hex/16, what about using the whole a-zA-Z0-9! 26 + 26 + 10, base 62.  As I researched base 62, found some pre-existing code for base 56 - removing letters and numbers commonly confused (eg - o O 0). Credits to Ross Duggan for the Base 56 encoding, who took inspiration from Python base 62 encoding scripts:  http://rossduggan.ie/blog/codetry/base-56-integer-encoding-in-php/index.html

### Improvements
With more time etc. etc. I'll leave that as something to talk about in person.

### Security Vulnerabilities

For demo purposes only. Not suitable for production.

### License

This is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
Base 56 encoding/decoding by Ross Duggan licensed under Creative Commons, CC BY-SA 3.0.
