<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UrlShortTest extends TestCase
{
    /**
     * Check home page loads
     *
     * @return void
     */
    public function testHomePage()
    {
        $this->get('/');
        $this->seeStatusCode(200);
    }

    /**
     * Check stats returns json with keys (values not checked)
     *
     * @return void
     */
    public function testStatistics()
    {
        $this->get('/api/statistics');
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'date_last_url_shortened',
            'last_url',
            'most_popular_destination',
            'most_requested_to_shorten',
        ]);
    }

    /**
     * Uses uniqid() to create a new abs URL "everytime" resulting in a create/201 response
     *
     * @return void
     */
    public function testCreateNewShortUrlAndCreateSameUrl()
    {
        $uniqueId = uniqid();
        $parameters = [
            'absolute_url' => 'https://'.$uniqueId.'bbc.co.uk',
        ];
        $this->post("/api/url-short", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure(
            [
                'absolute_url',
                'short_url',
            ]
        );

        /**
        * If same abs URL requested then just 200, don't create.
        */
        $this->post("/api/url-short", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonContains(
            [
                'absolute_url' => 'https://'.$uniqueId.'bbc.co.uk',
            ]
        );
    }
}
