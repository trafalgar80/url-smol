<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('url_stats')->insert([
            'id' => 1,
            'url_base56_count' => 1,
            'datetime_last_url' => '0000-00-00 00:00:00',
            'last_url' => '',
        ]);
    }
}
