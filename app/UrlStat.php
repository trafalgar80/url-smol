<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlStat extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url_base56_count', 'datetime_last_url', 'last_url',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
