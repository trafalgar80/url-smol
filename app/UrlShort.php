<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlShort extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'absolute_url', 'short_url', 'number_of_clicks', 'number_of_repeats',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * getShortUrl
     *
     * @param  mixed  $numberToConvert
     *
     * @return string
     */
    public function getShortUrl($numberToConvert)
    {
        $alphabet_raw = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
        $alphabet = str_split($alphabet_raw);

        return $this->base56_encode($numberToConvert, $alphabet);
    }

    /**
     * Credit to http://rossduggan.ie/blog/codetry/base-56-integer-encoding-in-php/index.html
     * @author Ross Duggan
     *
     * base56_encode
     *
     * @param  integer  $num
     * @param  mixed  $alphabet
     *
     * @return void
     */
    private function base56_encode($num, $alphabet){
        /*
        Encode a number in Base X

        `num`: The number to encode
        `alphabet`: The alphabet to use for encoding
        */
        if ($num == 0){
            return 0;
        }

        $n = str_split($num);
        $arr = array();
        $base = sizeof($alphabet);

        while($num){
            $rem = $num % $base;
            $num = (int)($num / $base);
            $arr[]=$alphabet[$rem];
        }

        $arr = array_reverse($arr);
        return implode($arr);
    }

    /**
     * Credit to http://rossduggan.ie/blog/codetry/base-56-integer-encoding-in-php/index.html
     * @author Ross Duggan
     *
     * base56_decode
     *
     * @param  string  $string
     * @param  array  $alphabet
     *
     * @return integer
     */
    private function base56_decode($string, $alphabet){
        /*
        Decode a Base X encoded string into the number

        Arguments:
        - `string`: The encoded string
        - `alphabet`: The alphabet to use for encoding
        */

        $base = sizeof($alphabet);
        $strlen = strlen($string);
        $num = 0;
        $idx = 0;

        $s = str_split($string);
        $tebahpla = array_flip($alphabet);

        foreach($s as $char){
            $power = ($strlen - ($idx + 1));
            $num += $tebahpla[$char] * (pow($base,$power));
            $idx += 1;
        }
        return $num;
    }
}
