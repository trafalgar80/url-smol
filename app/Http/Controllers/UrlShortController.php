<?php

namespace App\Http\Controllers;

use App\Author;
use App\UrlShort;
use App\UrlStat;
use Illuminate\Http\Request;

class UrlShortController extends Controller
{

    /**
     * Takes shortened url and redirects to long/absolute URL
     *
     * @param  string  $short_url
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function redirect($short_url)
    {
        $shortUrl = UrlShort::where('short_url', $short_url)->first();

        if (empty($shortUrl)) {

            return response('Not Found :(', 404);
        }

        $shortUrl->number_of_clicks += 1;
        $shortUrl->save();

        return redirect($shortUrl->absolute_url);
    }

    /**
     * Create shortened url and redirects to long/absolute URL
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function create(Request $request)
    {
        $absoluteUrl = $request->input('absolute_url');
        $shortUrl = UrlShort::where('absolute_url',$absoluteUrl)->first();
        $responseCode = 201;

        if (empty($shortUrl)) {
            $data = $request->all();
            $stats = UrlStat::find(1);
            $data['short_url'] = (new UrlShort())->getShortUrl($stats->url_base56_count+1000);
            $shortUrl = UrlShort::create($data);

            $stats->url_base56_count += 1;
            $stats->datetime_last_url = new \DateTime();
            $stats->last_url = $shortUrl->absolute_url;
            $stats->save();
        }
        else {
            $shortUrl->number_of_repeats +=1;
            $shortUrl->save();
            $responseCode = 200;
        }

        $result = [ 'short_url' => env('APP_URL').env('APP_PREFIX').$shortUrl->short_url, 'absolute_url' => $shortUrl->absolute_url ];
        return response()->json($result, $responseCode);
    }

    /**
     * Statistics, generated on the fly for demo, but usually done by cron etc.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function statistics()
    {
        //most clicked - max number_of_clicks
        $popularShortClicked = UrlShort::orderBy('number_of_clicks','desc')->first()->absolute_url;
        $popularUrlForShortening = UrlShort::orderBy('number_of_repeats','desc')->first()->absolute_url;
        $stats = UrlStat::find(1);
        $statsData = [
            'most_popular_destination' => $popularShortClicked,
            'most_requested_to_shorten' => $popularUrlForShortening,
            'date_last_url_shortened' => $stats->datetime_last_url,
            'last_url' => $stats->last_url,
        ];

        return response()->json($statsData);
    }

    /**
    * Home page
    *
    * @return \Illuminate\View\View
    */
    public function home()
    {
        return view('home');

    }

}
