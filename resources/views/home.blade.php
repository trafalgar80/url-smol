<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>URL Smol</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <style>
    img.resize {
        max-width: 100%;
        height: auto;
        border-radius: 8px;
        box-shadow: 0 13px 27px -5px hsla(240, 30.1%, 28%, 0.25), 0 8px 16px -8px hsla(0, 0%, 0%, 0.3), 0 -6px 16px -6px hsla(0, 0%, 0%, 0.03);
    }
    </style>
  </head>

  <body class="text-center">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h1>Smol URL</h1>
                <form class="form" id="form">
                        <img class="mb-4 resize" src="https://picsum.photos/280/200">
                        <div id="result" class="result"></div>
                        <label for="absolute_url" class="sr-only">Enter URL to shorten (must be complete with http..)</label>
                        <input type="url" id="absolute_url" name="absolute_url" class="form-control" placeholder="URL" required autofocus>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Go!</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div id="statistics" class="statistics text-left"></div>
                </div>
                <div class="col-md-4"></div>
        </div>
        <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <p class="mt-5 mb-3 text-muted">By Hakim Miah</p>
                </div>
                <div class="col-md-4"></div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
            var loadStats = function() {
                $.get( "/api/statistics", function( data ) {
                    $("#statistics").html("");
                    $("#statistics").append('<ul>');
                    $.each(data, function(k, v){
                        $("#statistics").append('<li>' + k + ': '+ v +'</li>');
                    });
                    $("#statistics").append("</ul>");
                });
            }
            $( "#form" ).submit(function( event ) {
                event.preventDefault();
                    abs_url = $( "#absolute_url" ).val();
                    $.post( "/api/url-short", { 'absolute_url':abs_url }, function( data ) {
                        $("#result").html('<a href="'+ data.short_url
                            +'" target="_blank">' + data.short_url + '</a> => '
                            + data.absolute_url);
                        loadStats();
                    });
                });
            $( document ).ready(function() {
                loadStats();
            });
        </script>
    </body>
</html>
